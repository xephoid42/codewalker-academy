var b = true;
b = 1 < 10;
b = 10 > 30;
b = !b;

if (b) {
	document.write("Hello World");
}

if (0 == 10) {
	document.write("Nope!");
}

if (7 < 1004 && !b) {
	document.write("Nope!");
}

if (7 < 1004 || !b) {
	document.write("Yup!");
}

if (0 == 10 && 7 < 1004 || !b) {
	document.write("Yup!");
}

var string = "Something";

if (string == "something" || string == "Something ") {
	document.write("Nope!");
}

if (string.length > 10) {
	// Nope!
} else {
	document.write("Yup!");
}
